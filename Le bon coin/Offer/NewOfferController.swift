//
//  Project > Le bon coin
//  Filename > NewOfferController.swift
//
//  Created by Guillaume Gonzales on 04/10/2018.
//  Copyright © 2018 Tokidev S.A.S. - All rights reserved.
//

import UIKit
import Firebase

class NewOfferController: UIViewController  {
    
    let titleTF: UITextField = {
        let txt = UITextField()
        txt.placeholder = "Title"
        txt.font = UIFont.systemFont(ofSize: 14)
        txt.borderStyle = .roundedRect
        txt.autocapitalizationType = .none
        txt.addTarget(self, action: #selector(handleTextChange), for: .editingChanged)
        return txt
    }()
    
    let categoryTF: UITextField = {
        let txt = UITextField()
        txt.font = UIFont.systemFont(ofSize: 14)
        txt.borderStyle = .roundedRect
        txt.autocapitalizationType = .none
        txt.addTarget(self, action: #selector(handleTextChange), for: .editingChanged)
        txt.placeholder = "Category"
        return txt
    }()
    
    let cityTF: UITextField = {
        let txt = UITextField()
        txt.font = UIFont.systemFont(ofSize: 14)
        txt.borderStyle = .roundedRect
        txt.autocapitalizationType = .none
        txt.addTarget(self, action: #selector(handleTextChange), for: .editingChanged)
        txt.placeholder = "City"
        return txt
    }()
    
    let offerTF: UITextField = {
        let txt = UITextField()
        txt.font = UIFont.systemFont(ofSize: 14)
        txt.borderStyle = .roundedRect
        txt.autocapitalizationType = .none
        txt.addTarget(self, action: #selector(handleTextChange), for: .editingChanged)
        txt.placeholder = "Offer"
        return txt
    }()
    
    let priceTF: UITextField = {
        let txt = UITextField()
        txt.keyboardType = .numberPad
        txt.font = UIFont.systemFont(ofSize: 14)
        txt.borderStyle = .roundedRect
        txt.autocapitalizationType = .none
        txt.addTarget(self, action: #selector(handleTextChange), for: .editingChanged)
        txt.placeholder = "Price"
        return txt
    }()
    
    let btnValider : UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Create", for: .normal)
        button.backgroundColor = #colorLiteral(red: 0.9372549057, green: 0.3490196168, blue: 0.1921568662, alpha: 1)
        button.setTitleColor(.white, for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        button.layer.cornerRadius = 5
        button.addTarget(self, action: #selector(handleOffer), for: .touchUpInside)
        
        
        // button est desactivé au debut du remplissage
        button.isEnabled = false
        
        
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        setupNavigationBarTitle()
        setupForm()
    }
    
    
    fileprivate func setupNavigationBarTitle() {
        let titleImageView = UIImageView(image: #imageLiteral(resourceName: "logo").withRenderingMode(.alwaysOriginal))
        titleImageView.contentMode = .scaleAspectFit
        titleImageView.clipsToBounds = true
        navigationItem.titleView = titleImageView
    }
    
    fileprivate func setupForm() {
        view.addSubview(titleTF)
        view.addSubview(categoryTF)
        view.addSubview(cityTF)
        view.addSubview(offerTF)
        view.addSubview(priceTF)
        view.addSubview(btnValider)
        
        titleTF.anchor(top: view.safeAreaLayoutGuide.topAnchor, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor, paddingTop: 8, paddingLeft: 8, paddingBottom: 0, paddingRight: 8, width: 0, height: 30)
        
        categoryTF.anchor(top: titleTF.bottomAnchor, leading: titleTF.leadingAnchor, bottom: nil, trailing: titleTF.trailingAnchor, paddingTop: 8, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 30)
        
        cityTF.anchor(top: categoryTF.bottomAnchor, leading: titleTF.leadingAnchor, bottom: nil, trailing: titleTF.trailingAnchor, paddingTop: 8, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 30)
        
        offerTF.anchor(top: cityTF.bottomAnchor, leading: titleTF.leadingAnchor, bottom: nil, trailing: titleTF.trailingAnchor, paddingTop: 8, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 30)
        
        priceTF.anchor(top: offerTF.bottomAnchor, leading: titleTF.leadingAnchor, bottom: nil, trailing: titleTF.trailingAnchor, paddingTop: 8, paddingLeft: 0, paddingBottom:0, paddingRight: 0, width: 0, height: 30)
        
        btnValider.anchor(top: priceTF.bottomAnchor, leading: titleTF.leadingAnchor, bottom: nil, trailing: titleTF.trailingAnchor, paddingTop: 8, paddingLeft: 0, paddingBottom: 8, paddingRight: 0, width: 0, height: 30)
        
    }
    
    @objc func handleTextChange() {
        // au changement du text activer le button creation
        btnValider.isEnabled = true
    }
    
    @objc func handleOffer() {
        
        
        // a l'envoie des information je desactive le button creation
        btnValider.isEnabled = false
        
        guard let title = titleTF.text, !title.isEmpty else { return }
        guard let category = categoryTF.text, !category.isEmpty else { return }
        guard let city = cityTF.text, !city.isEmpty else { return }
        guard let offer = offerTF.text, !offer.isEmpty else { return }
        guard let price = priceTF.text, !price.isEmpty else { return }
        
        // traitement en cas de non saisie de number afficher message en console , pop pas encore faite
        var num = Double(price)
        if num == nil {
            
            // en cas d'erreur j'active
            btnValider.isEnabled = true
            print("Invalid Price")
            return
        }
        
        let values = ["category": category,
                      "title": title,
                      "city": city,
                      "price": price,
                      "offer": offer] as [String: Any]
        
        let ref = Database.database().reference().child("offers").childByAutoId()
        ref.updateChildValues(values) { (err, ref) in
            
            if let err = err {
                print("Failed adding new offer:", err)
                return
            }
            
            // vider les champs
            self.viderChamps()
            
            // ici pop
            self.affichePOP()
            
        }
    }
    
    private func affichePOP(){
        // Set title, message and alert style
        let alertController = UIAlertController(title: "Le bon coin", message: "Offer Added", preferredStyle: .alert)
        
        // You can add plural action.
        let okAction = UIAlertAction(title: "OK" , style: .default){
            action in
            // switch vers l'affichage de la liste des annonces
            self.tabBarController?.selectedIndex = 0
        }
        
        
        // Add the action.
        alertController.addAction(okAction)
        
        // Show alert
        present(alertController, animated: true, completion: nil)
        
        
    }
    
    private func viderChamps(){
        btnValider.isEnabled = false
        titleTF.text = ""
        cityTF.text = ""
        priceTF.text = ""
        categoryTF.text = ""
        offerTF.text = ""
    }
    
}

