//
//  Annonce.swift
//  Le bon coin
//
//  Created by mbdse on 17/10/2019.
//  Copyright © 2019 Tokidev S.A.S. All rights reserved.
//

import Foundation

struct Offer {
    var id: String
    var category : String
    var title : String
    var city: String
    var offer: String
    var price : String
    
    // constructeur normal
    init( _id: String, _category : String, _title : String, _city : String, _offer : String, _price : String) {
        self.id = _id
        self.category = _category
        self.title = _title
        self.city = _city
        self.offer = _offer
        self.price = _price
    }
    
    // constructeur en utilisant dictionnary
    init(id: String, dictionary: [String: Any]) {
        self.id = id
        self.offer = dictionary["offer"] as? String ?? ""
        self.title = dictionary["title"] as? String ?? ""
        self.city = dictionary["city"] as? String ?? ""
        self.price = dictionary["price"] as? String ?? "0"
        self.category = dictionary["category"] as? String ?? ""
    }
    
}
