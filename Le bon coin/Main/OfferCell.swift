//
//  AnnonceCell.swift
//  Le bon coin
//
//  Created by mbdse on 17/10/2019.
//  Copyright © 2019 Tokidev S.A.S. All rights reserved.
//
import UIKit
import Firebase

class OfferCell: UICollectionViewCell {
    
    var annonce: Offer? {
        didSet {
            // execution de cette fonction
            setAnnonce()
        }
    }
    
    let titleLabel: UILabel = {
        let label = UILabel()
        label.text = "Default Title"
        label.font = UIFont.boldSystemFont(ofSize: 14)
        label.textColor = #colorLiteral(red: 0.9372549057, green: 0.3490196168, blue: 0.1921568662, alpha: 1)
        return label
    }()
    
    let offerLabel: UILabel = {
        let label = UILabel()
        label.text = "Default Offer"
        label.font = UIFont.systemFont(ofSize: 12)
        label.numberOfLines = 0
        return label
    }()
    
    let priceLabel: UILabel = {
        let label = UILabel()
        label.text = "Default Price"
        label.font = UIFont.systemFont(ofSize: 12)
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        // initialisation de element de notre viewcell
        setupCellUI()
    }
    
    fileprivate func setupCellUI() {
        addSubview(titleLabel)
        addSubview(offerLabel)
        addSubview(priceLabel)
        
        titleLabel.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor, paddingTop: 8, paddingLeft: 8, paddingBottom: 0, paddingRight: 8, width: 0, height: 0)
        
        offerLabel.anchor(top: titleLabel.bottomAnchor, leading: titleLabel.leadingAnchor, bottom: nil, trailing: titleLabel.trailingAnchor, paddingTop: 8, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 0)
        
        priceLabel.anchor(top: offerLabel.bottomAnchor, leading: titleLabel.leadingAnchor, bottom: bottomAnchor, trailing: titleLabel.trailingAnchor, paddingTop: 8, paddingLeft: 0, paddingBottom: 8, paddingRight: 0, width: 0, height: 0)
    }
    
    fileprivate func setAnnonce() {
        guard let annonce = annonce else { return }
        
        titleLabel.text = annonce.title
        offerLabel.text = annonce.offer
        priceLabel.text = "\(annonce.price)€"
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
