//
//  Project > Le bon coin
//  Filename > ViewController.swift
//
//  Created by Guillaume Gonzales on 04/10/2018.
//  Copyright © 2018 Tokidev S.A.S. - All rights reserved.
//

import UIKit
import Firebase

class HomeController: UICollectionViewController, UICollectionViewDelegateFlowLayout {

    let cellId = "cellId"
    
    var offers = [Offer]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // couleur du backgroundColor
        collectionView.backgroundColor = .gray
        
        collectionView.register(OfferCell.self, forCellWithReuseIdentifier: cellId)
        
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(handleRefresh), for: .valueChanged)
        collectionView?.refreshControl = refreshControl
        
        setupNavigationBarTitle()
        fetchOffers()
    }
    
    // j'ai essayé dans ce point a faire le refresh apres l'insertion d'annonce et le switch vers home controller
    override func viewDidAppear(_ animated: Bool) {
        // il fait un reload de la liste
        fetchOffers()
    }
    

    fileprivate func setupNavigationBarTitle() {
        let titleImageView = UIImageView(image: #imageLiteral(resourceName: "logo").withRenderingMode(.alwaysOriginal))
        titleImageView.contentMode = .scaleAspectFit
        titleImageView.clipsToBounds = true
        navigationItem.titleView = titleImageView
    }
    
    @objc func handleRefresh() {
        fetchOffers()
    }
    
    fileprivate func fetchOffers() {
        offers.removeAll()
        
        let ref = Database.database().reference().child("offers")
        ref.observeSingleEvent(of: .value, with: { (snapshot) in
            self.collectionView.refreshControl?.endRefreshing()
            
            guard let allOffersDict = snapshot.value as? [String: Any] else { return }
            
            
            allOffersDict.forEach({ (oid, offerValue) in
                guard let offerDic = offerValue as? [String: Any] else { return }
                let offer = Offer(id: oid, dictionary: offerDic)
                self.offers.insert(offer, at: 0)
                
                // mettre les annonces par ordre alphabetique du titre
                self.offers.sort(by: { (p1, p2) -> Bool in
                    return p1.title.compare(p2.title) == .orderedAscending
                })
                
            })
            
            DispatchQueue.main.async {
                self.collectionView.reloadData()
            }
            
        }) { (err) in
            print("Failed fetching offers:", err)
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return offers.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! OfferCell
        cell.annonce = offers[indexPath.item]
        cell.backgroundColor = .white
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: 120)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }

}

